<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="/WEB-INF/jsp/header.jsp" />

<a href="${linkTo[CondominioController].form()}">Novo Condominio</a>
<table class="table table-hover">
	<thead>
		<tr>
			<th>Id</th>
			<th>Tipo de Apartamento</th>
			<th>Data da Despesa</th>
			<th>Data de Pagamento</th>
			<th>Valor Pago</th>
			<th>Remover</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${condominios}" var="c">
			<c:set var="id" value="${c.id}" scope="request"></c:set>
			<tr>
				<td>${c.id}</td>
				<td>${c.tipoApto}</td>
				<td>${c.dtDespesa}</td>
				<td>${c.dtPag}</td>
				<td>${c.valorPago}</td>

				<td><a href="${linkTo[CondominioController].edita(c.id)}">
					<span class="glyphicon glyphicon-pencil"></span>
					</a>
				</td>
				<td>
					  <form action="${linkTo[CondominioController].apaga(c.id)}" method="POST">
					 <button class="btn btn-link" type="submit" name="_method" value="DELETE">
						<span class="glyphicon glyphicon-trash"></span>				 
					 </button>
					</form> 
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<c:import url="/WEB-INF/jsp/footer.jsp" />