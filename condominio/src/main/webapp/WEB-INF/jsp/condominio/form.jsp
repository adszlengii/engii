<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Condominio</title>

    <link href="<c:url value="/css/bootstrap.min.css" />" rel="stylesheet">
  </head>
  <body>
  <div class="container">
    <h1>Condominio</h1>
    
    <br>
    
	<form action="${linkTo[CondominioController].adiciona(null)}" method="post">
		<div class="row">
			<div class="col-sm-3">	
				<div class="form-group">
					<label for="tipoApto">*Tipo de Apartamento</label>
					<input id="tipoApto" class="form-control" type="text" 
					name="condominio.tipoApto" value="${condominio.tipoApto}">
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group">
					<label for=dtDespesa>*Data da Despesa</label>
					<input id="dtDespesa" class="form-control" type="text" 
					name="condominio.dtDespesa" 
					value="<fmt:formatDate pattern="dd/MM/yyyy"  value="${condominio.dtDespesa.time}"/>">
				</div>
			</div>
		<div class="col-sm-3">
				<div class="form-group">
					<label for=dtPag>*Data de Pagamento</label>
					<input id="dtPag" class="form-control" type="text" 
					name="condominio.dtPag" 
					value="<fmt:formatDate pattern="dd/MM/yyyy"  value="${condominio.dtPag.time}"/>">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">	
				<div class="form-group">
					<label for="valorPago">*Valor Pago</label>
					<input id="valorPago" class="form-control" type="number" 
					name="condominio.valorPago" value="${condominio.valorPago}">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-9">
				<button class="btn btn-primary pull-right" type="submit">Salvar</button>
			</div>
		</div>
	</form>
	</div>

    <script src="<c:url value="/js/jquery.min.js" />"></script>
    <script src="<c:url value="/js/bootstrap.min.js" />"></script>
  </body>
</html>