<div class="container">
    <h1>Despesas</h1>
    
    <br>
    
	<form action="${linkTo[DespesasController].adiciona(null)}" method="post">
		<div class="row">
			<div class="col-sm-3">	
				<div class="form-group">
					<label for="valor">*Valor da Despesa</label>
					<input id="valor" class="form-control" type="text" 
					name="apto.valor" value="${despesas.valor}">
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group">
					<label for=descricao>*Descri��o</label>
					<input id="descricao" class="form-control" type="text" 
					name="despesas.descricao" value="${despesas.descricao}">
				</div>
			</div>
		<div class="col-sm-3">
				<div class="form-group">
					<label for=despEspec>*Despesa Espec�fica</label>
					<input id="despEspec" class="form-control" type="text" 
					name="despesas.despEspec" value="${despesas.despEspec}">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-9">
				<button class="btn btn-primary pull-right" type="submit">Salvar</button>
			</div>
		</div>
	</form>
	</div>

    <script src="<c:url value="/js/jquery.min.js" />"></script>
    <script src="<c:url value="/js/bootstrap.min.js" />"></script>
  </body>
</html>