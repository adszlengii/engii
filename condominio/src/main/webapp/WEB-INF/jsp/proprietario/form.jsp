<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Proprietario</title>

<link href="<c:url value="/css/bootstrap.min.css" />" rel="stylesheet">
</head>
<body>
	<div class="container">
		<h1>Proprietario</h1>

		<br>

		<form action="${linkTo[PropController].adiciona(null)}" method="post">
			<div class="row">
				<div class="col-sm-3">
					<div class="form-group">
						<label for="nomeProp">*Nome do Proprietario</label> <input
							id="nomeProp" class="form-control" type="text"
							name="prop.nomeProp" value="${prop.nomeProp}">
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<label for=telProp>*Telefone do Proprietario</label> <input
							id="telProp" class="form-control" type="text" name="prop.telProp"
							value="${prop.telProp}">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-9">
					<button class="btn btn-primary pull-right" type="submit">Salvar</button>
				</div>
			</div>
		</form>
	</div>

	<script src="<c:url value="/js/jquery.min.js" />"></script>
	<script src="<c:url value="/js/bootstrap.min.js" />"></script>
</body>
</html>