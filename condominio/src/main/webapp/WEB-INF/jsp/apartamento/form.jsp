<div class="container">
    <h1>Apartamento</h1>
    
    <br>
    
	<form action="${linkTo[AptoController].adiciona(null)}" method="post">
		<div class="row">
			<div class="col-sm-3">	
				<div class="form-group">
					<label for="numApto">*N�mero de Apartamentos</label>
					<input id="numApto" class="form-control" type="number" 
					name="apto.numApto" value="${apto.numApto}">
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group">
					<label for=qtdQuartos>*Quantidade de Quartos</label>
					<input id="qtdQuartos" class="form-control" type="text" 
					name="apto.qtdQuartos" value="${apto.qtdQuartos}">
				</div>
			</div>
		<div class="col-sm-3">
				<div class="form-group">
					<label for=tipoOcup>*Tipo de Ocupa��o</label>
					<input id="tipoOcup" class="form-control" type="text" 
					name="apto.tipoOcup" value="${apto.tipoOcup}">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-9">
				<button class="btn btn-primary pull-right" type="submit">Salvar</button>
			</div>
		</div>
	</form>
	</div>

    <script src="<c:url value="/js/jquery.min.js" />"></script>
    <script src="<c:url value="/js/bootstrap.min.js" />"></script>
  </body>
</html>