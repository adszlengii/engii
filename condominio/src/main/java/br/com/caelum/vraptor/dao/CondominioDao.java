package br.com.caelum.vraptor.dao;

import java.util.List;


import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import br.com.caelum.vraptor.model.Condominio;

public class CondominioDao {

	private EntityManager manager;


	public CondominioDao(EntityManager manager) {
		this.manager = manager;
	}
	

	public List<Condominio> lista() {
		TypedQuery<Condominio> query = manager.createQuery("select c from Condominio c", Condominio.class);
		return query.getResultList();
	}
	@Transactional
	public void adiciona(Condominio condominio) {
		if (condominio.getId() == 0) {
			manager.persist(condominio);
		} else {
			manager.merge(condominio);
		}
	}

	public Condominio busca(long id) {
		return manager.find(Condominio.class, id);
	}
	
	public void deleta(long id) {
		manager.remove(manager.getReference(Condominio.class, id));
	}
}
