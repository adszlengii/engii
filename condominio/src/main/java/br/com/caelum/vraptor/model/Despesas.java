package br.com.caelum.vraptor.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

	@Entity
	public class Despesas {

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private long id;
		@NotEmpty
		private int valor;
		@NotNull
		private String descricao;
		@NotEmpty
		private String despEspec;
		
		
		public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
		public int getValor() {
			return valor;
		}
		public void setValor(int valor) {
			this.valor = valor;
		}
		public String getDescricao() {
			return descricao;
		}
		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		public String getDespEspec() {
			return despEspec;
		}
		public void setDespEspec(String despEspec) {
			this.despEspec = despEspec;
		}	

		

}
