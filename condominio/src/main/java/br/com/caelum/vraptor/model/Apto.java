package br.com.caelum.vraptor.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Apto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@NotEmpty
	private int numApto;
	@NotEmpty
	private int qtdQuartos;
	@NotNull
	private String tipoOcup;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getNumApto() {
		return numApto;
	}
	public void setNumApto(int numApto) {
		this.numApto = numApto;
	}
	public int getQtdQuartos() {
		return qtdQuartos;
	}
	public void setQtdQuartos(int qtdQuartos) {
		this.qtdQuartos = qtdQuartos;
	}
	public String getTipoOcup() {
		return tipoOcup;
	}
	public void setTipoOcup(String tipoOcup) {
		this.tipoOcup = tipoOcup;
	}








}