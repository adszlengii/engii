package br.com.caelum.vraptor.controller;

import java.util.List;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.dao.DespesasDao;
import br.com.caelum.vraptor.interceptor.IncludeParameters;
import br.com.caelum.vraptor.model.Despesas;
import br.com.caelum.vraptor.validator.Validator;

@Controller
public class DespesasController {

	private DespesasDao despesasDao;
	private Validator validator;
	private Result result;
	
	public DespesasController() {
		
	}

	
	public DespesasController(DespesasDao despesasDao, Validator validator, Result result) {
		super();
		this.despesasDao = despesasDao;
		this.validator = validator;
		this.result = result;
	}


	public void form() {
		
	}
	
	@IncludeParameters
	@Post
	public void adiciona(Despesas despesas) {
		validator.onErrorRedirectTo(this).form();
		despesasDao.adiciona(despesas);
		result.redirectTo(this).lista();
	}

	public void lista() {
		List<Despesas> despesas = despesasDao.lista();
		result.include("despesas", despesas);
	}
}

