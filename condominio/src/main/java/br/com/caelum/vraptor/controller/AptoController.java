package br.com.caelum.vraptor.controller;

import java.util.List;



import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.dao.AptoDao;
import br.com.caelum.vraptor.interceptor.IncludeParameters;
import br.com.caelum.vraptor.model.Apto;
import br.com.caelum.vraptor.validator.Validator;

@Controller
public class AptoController {

	private AptoDao aptoDao;
	private Validator validator;
	private Result result;
	
	public AptoController() {
		
	}

	
	public AptoController(AptoDao aptoDao, Validator validator, Result result) {
		super();
		this.aptoDao = aptoDao;
		this.validator = validator;
		this.result = result;
	}


	public void form() {
		
	}
	
	@IncludeParameters
	@Post
	public void adiciona(Apto apto) {
		validator.onErrorRedirectTo(this).form();
		aptoDao.adiciona(apto);
		result.redirectTo(this).lista();
	}

	public void lista() {
		List<Apto> aptos = aptoDao.lista();
		result.include("aptos", aptos);
	}
}
