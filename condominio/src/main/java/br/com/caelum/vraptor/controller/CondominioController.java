package br.com.caelum.vraptor.controller;

import java.util.List;



import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.dao.CondominioDao;
import br.com.caelum.vraptor.interceptor.IncludeParameters;
import br.com.caelum.vraptor.model.Condominio;
import br.com.caelum.vraptor.validator.Validator;

@Controller
public class CondominioController {

	private CondominioDao condominioDao;
	private Validator validator;
	private Result result;
	
	public CondominioController() {
		
	}

	
	public CondominioController(CondominioDao condominioDao, Validator validator, Result result) {
		super();
		this.condominioDao = condominioDao;
		this.validator = validator;
		this.result = result;
	}


	public void form() {
		
	}
	
	@IncludeParameters
	@Post
	public void adiciona(Condominio condominio) {
		validator.onErrorRedirectTo(this).form();
		condominioDao.adiciona(condominio);
		result.redirectTo(this).lista();
	}

	public void lista() {
		List<Condominio> condominios = condominioDao.lista();
		result.include("condominios", condominios);
	}
}
