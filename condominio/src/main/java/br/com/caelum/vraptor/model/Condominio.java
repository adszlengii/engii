package br.com.caelum.vraptor.model;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;



@Entity
public class Condominio {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@NotEmpty
	private String tipoApto;
	@NotNull
	@Temporal(TemporalType.DATE)
	private Calendar dtDespesa;
	@NotEmpty
	private double valorPago;
	@NotNull
	@Temporal(TemporalType.DATE)
	private Calendar dtPag;

	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTipoApto() {
		return tipoApto;
	}
	public void setTipoApto(String tipoApto) {
		this.tipoApto = tipoApto;
	}
	public Calendar getDtDespesa() {
		return dtDespesa;
	}
	public void setDtNasc(Calendar dtDespesa) {
		this.dtDespesa = dtDespesa;
	}
	public Calendar getDtPag() {
		return dtPag;
	}
	public void setDtPag(Calendar dtPag) {
		this.dtPag = dtPag;
	}
	public double getValorPago() {
		return valorPago;
	}
	public void setValorPago(double valorPago) {
		this.valorPago = valorPago;
	}


	
	
}
