package br.com.caelum.vraptor.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import br.com.caelum.vraptor.model.Prop;

public class PropDao {

	private EntityManager manager;


	public PropDao(EntityManager manager) {
		this.manager = manager;
	}
	

	public List<Prop> lista() {
		TypedQuery<Prop> query = manager.createQuery("select p from Prop p", Prop.class);
		return query.getResultList();
	}
	@Transactional
	public void adiciona(Prop prop) {
		if (prop.getId() == 0) {
			manager.persist(prop);
		} else {
			manager.merge(prop);
		}
	}

	public Prop busca(long id) {
		return manager.find(Prop.class, id);
	}
	
	public void deleta(long id) {
		manager.remove(manager.getReference(Prop.class, id));
	}
}

