package br.com.caelum.vraptor.dao;

import java.util.List;


import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import br.com.caelum.vraptor.model.Apto;

public class AptoDao {

	private EntityManager manager;


	public AptoDao(EntityManager manager) {
		this.manager = manager;
	}
	

	public List<Apto> lista() {
		TypedQuery<Apto> query = manager.createQuery("select a from Apto a", Apto.class);
		return query.getResultList();
	}
	@Transactional
	public void adiciona(Apto apto) {
		if (apto.getId() == 0) {
			manager.persist(apto);
		} else {
			manager.merge(apto);
		}
	}

	public Apto busca(long id) {
		return manager.find(Apto.class, id);
	}
	
	public void deleta(long id) {
		manager.remove(manager.getReference(Apto.class, id));
	}
}
