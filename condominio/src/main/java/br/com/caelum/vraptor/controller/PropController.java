package br.com.caelum.vraptor.controller;

import java.util.List;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.dao.PropDao;
import br.com.caelum.vraptor.interceptor.IncludeParameters;
import br.com.caelum.vraptor.model.Prop;
import br.com.caelum.vraptor.validator.Validator;

	@Controller
	public class PropController {

		private PropDao propDao;
		private Validator validator;
		private Result result;
		
		public PropController() {
			
		}

		
		public PropController(PropDao propDao, Validator validator, Result result) {
			super();
			this.propDao = propDao;
			this.validator = validator;
			this.result = result;
		}


		public void form() {
			
		}
		
		@IncludeParameters
		@Post
		public void adiciona(Prop prop) {
			validator.onErrorRedirectTo(this).form();
			propDao.adiciona(prop);
			result.redirectTo(this).lista();
		}

		public void lista() {
			List<Prop> props = propDao.lista();
			result.include("proprietarios", props);
		}
	}

