package br.com.caelum.vraptor.dao;



import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import br.com.caelum.vraptor.model.Despesas;

public class DespesasDao {

	private EntityManager manager;


	public DespesasDao(EntityManager manager) {
		this.manager = manager;
	}
	

	public List<Despesas> lista() {
		TypedQuery<Despesas> query = manager.createQuery("select a from Despesas a", Despesas.class);
		return query.getResultList();
	}
	@Transactional
	public void adiciona(Despesas despesas) {
		if (despesas.getId() == 0) {
			manager.persist(despesas);
		} else {
			manager.merge(despesas);
		}
	}

	public Despesas busca(long id) {
		return manager.find(Despesas.class, id);
	}
	
	public void deleta(long id) {
		manager.remove(manager.getReference(Despesas.class, id));
	}
}

